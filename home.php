<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            height: 100%;
            margin: 0;
            padding: 0;
            display: flex;
            flex-direction: column;
            align-items: center;
            font-family: Arial, sans-serif;
            background-color: #f8f8f8; 
            color: #333;
            padding-top: 60px; 
        }
        nav.navbar {
            width: 100%;
            position: fixed;
            top: 0;
            z-index: 1000;
        }
        .navbar-collapse {
            justify-content: center;
        }

    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.php">Return to Search</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" href="#">Home</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container mt-5">
        <?php
        include 'db_connection.php';

        // La requête récupère les données et les trie par 'id' en ordre décroissant
        $sql = "SELECT * FROM movie_details ORDER BY id DESC";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "<div class='movie mb-4 p-3 shadow'>";
                echo "<img src='" . htmlspecialchars($row['image']) . "' alt='Movie Image' style='max-width: 100px; height: auto; display: block;'>";
                echo "<h2>" . htmlspecialchars($row['title']) . "</h2>";
                echo "<p>Année de sortie: " . htmlspecialchars($row['year']) . "</p>";
                echo "<p>Note des utilisateurs: " . htmlspecialchars($row['rating']) . "</p>";
                echo "<p>Votre note: " . htmlspecialchars($row['my_rating']) . "</p>";
                echo "<p>Commentaire: " . htmlspecialchars($row['my_comment']) . "</p>";
                echo "</div>";
            }
        } else {
            echo "<p>Aucun film trouvé. Commencez à ajouter des films!</p>";
        }
        $conn->close();
        ?>
    </div>
</body>
</html>