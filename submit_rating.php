<?php
include 'db_connection.php'; 

// Vérifier si le formulaire a été soumis
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Récupérer les données du formulaire
    $filmId = $_POST['film_id'];
    $title = $_POST['title'];
    $year = $_POST['year'];
    $actors = $_POST['actors'];
    $userRating = $_POST['user_rating'];
    $myRating = $_POST['my_rating'];
    $comment = $_POST['my_comment'];
    $image = $_POST['image']; 

    // Préparer la requête SQL pour insérer les données
    $sql = "INSERT INTO movie_details (id, title, year, actors, rating, my_rating, my_comment, image) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    // Préparer la déclaration
    if ($stmt = $conn->prepare($sql)) {
        // Lier les paramètres
        $stmt->bind_param("isssddss", $filmId, $title, $year, $actors, $userRating, $myRating, $comment, $image);

        // Exécuter la déclaration
        if ($stmt->execute()) {
            header('Location: home.php');
            exit; 
        } else {
            echo "Erreur lors de l'ajout des données : " . $stmt->error;
        }

        // Fermer la déclaration
        $stmt->close();
    } else {
        echo "Erreur lors de la préparation de la déclaration : " . $conn->error;
    }

    // Fermer la connexion
    $conn->close();
}
?>
