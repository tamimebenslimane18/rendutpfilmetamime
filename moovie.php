<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Résultats de Recherche IMDB</title>
    <!-- Intégration de Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
    body {
        height: 100%;
        margin: 0;
        padding: 0;
        display: flex;
        flex-direction: column;
        align-items: center;
        font-family: Arial, sans-serif;
        background-color: #f8f8f8; 
        color: #333;
        padding-top: 60px; 
    }
    nav.navbar {
        width: 100%;
        position: fixed;
        top: 0;
        z-index: 1000;
    }
    .navbar-collapse {
        justify-content: center;
    }
    .container {
        text-align: center;
        max-width: 600px; 
        margin-top: 20px;
        width: 100%; 
        padding: 20px;
        box-shadow: 0 8px 16px rgba(0, 0, 0, 0.1); 
    }
    h3 {
        margin-top: 20px; 
        margin-bottom: 10px; 
        font-weight: bold; 
        text-align: center; 
    }
    input[type="number"]::-webkit-inner-spin-button,
    input[type="number"]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    input[type="number"] {
     -moz-appearance: textfield;
    }
    input#rating {
        width: 80px;
        text-align: center; 
        margin-left:240px;
    }
    .details {
        margin-top: 20px;
        padding: 20px;
        box-shadow: 0 8px 16px rgba(0, 0, 0, 0.1); 
    }
    .actors-grid {
        display: grid;
        grid-template-columns: repeat(2, 1fr); 
        grid-gap: 10px;
        margin-top: 20px;
    }
</style>

</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.php">Search</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="home.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" aria-disabled="true">Disabled</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <?php
    include 'db_connection.php';  
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Récupérer les données du formulaire
            $filmId = $_POST['film_id'];
            $title = $_POST['title'];
            $year = $_POST['year'];
            $actors = $_POST['actors'];
            $userRating = $_POST['user_rating'];
            $myRating = $_POST['my_rating'];
            $comment = $_POST['my_comment'];
        
            //requête SQL
            $sql = "INSERT INTO movie_details (id, title, year, actors, rating, my_rating, my_comment) VALUES (?, ?, ?, ?, ?, ?, ?)";
            
            $stmt = $conn->prepare($sql);
            if ($stmt === false) {
                die('MySQL prepare error: ' . $conn->error);
            }
        
            // Lier les paramètres
            $stmt->bind_param("isssdds", $filmId, $title, $year, $actors, $userRating, $myRating, $comment);
        
            // Exécution de la requête
            if ($stmt->execute()) {
                echo "Enregistrement réussi.";
            } else {
                echo "Erreur d'enregistrement : " . $stmt->error;
            }
        
            $stmt->close();
            $conn->close();
        }

        if (isset($_GET["id"]) && isset($_GET["title"])) {
            $filmId = htmlspecialchars($_GET["id"]);
            $title = htmlspecialchars(urldecode($_GET["title"]));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://www.imdb.com/title/" . $filmId);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $htmlContent = curl_exec($ch);
            curl_close($ch);

            $imageRegex = '/<img alt="[^"]*" class="ipc-image" loading="eager" src="(.*?)"/';
            preg_match($imageRegex, $htmlContent, $imageMatches);
            $imageUrl = $imageMatches[1] ?? 'default_image.jpg';

            $releaseYearRegex = '/<a class="ipc-link ipc-link--baseAlt ipc-link--inherit-color" role="button" tabindex="0" aria-disabled="false" href="\/title\/tt\d+\/releaseinfo\?ref_=tt_ov_rdat">(.*?)<\/a>/';
            preg_match($releaseYearRegex, $htmlContent, $releaseYearMatches);
            $year = $releaseYearMatches[1] ?? 'N/A';

            $ratingRegex = '/<span class="sc-bde20123-1 cMEQkK">(.*?)<\/span>/';
            preg_match($ratingRegex, $htmlContent, $ratingMatches);
            $rating = $ratingMatches[1] ?? 'N/A';

            $actorsRegex = '/<a data-testid="title-cast-item__actor" href="\/name\/nm\d+\/\?ref_=tt_cl_t_\d+" class="sc-bfec09a1-1 gCQkeh">(.*?)<\/a>/';
            preg_match_all($actorsRegex, $htmlContent, $actorsMatches);
            $actors = $actorsMatches[1] ?? [];

            echo "<div class='container'>";
            echo "<h1>{$title}</h1>";
            echo "<img src='{$imageUrl}' alt='Image de {$title}' style='max-width: 100%; height: auto; object-fit: contain;'>";
            echo "<div class='details'>";
            echo "<p>Année de sortie: {$year}</p>";
            echo "<p>Note des utilisateurs: {$rating}</p>";
            echo "<h3 style='text-align:center; width:100%;'>Acteurs</h3>"; 
            echo "<div class='actors-grid'>";
            foreach ($actors as $actor) {
                echo "<div>{$actor}</div>";
            }
            echo "</div>";
            echo "</div>";
            echo "</div>";
        } else {
            echo "<p>ID du film non fourni.</p>";
        }
        
    ?>
    <div class="container">
    <h2>Donnez votre avis</h2>
    <form action="submit_rating.php" method="post">
        <input type="hidden" name="film_id" value="<?php echo $filmId; ?>">
        <input type="hidden" name="title" value="<?php echo $title; ?>">
        <input type="hidden" name="year" value="<?php echo $year; ?>">
        <input type="hidden" name="actors" value="<?php echo implode(', ', $actors); ?>">
        <input type="hidden" name="user_rating" value="<?php echo $rating; ?>">
        <input type='hidden' name='image' value='<?php echo $imageUrl; ?>'>
        <div class="mb-3">
            <label for="rating" class="form-label">Votre note :</label>
            <input type="number" class="form-control" id="rating" name="my_rating" min="1" max="10" required>
        </div>
        <div class="mb-3">
            <label for="comment" class="form-label">Votre commentaire :</label>
            <textarea class="form-control" id="comment" name="my_comment" rows="4" required></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Soumettre</button>
    </form>
</div>

</body>
</html>
