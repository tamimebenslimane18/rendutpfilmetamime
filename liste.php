<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Résultats de Recherche IMDB</title>
    <!-- Intégration de Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
    body {
        height: 100%;
        margin: 0;
        padding: 0;
        display: flex;
        flex-direction: column;
        align-items: center;
        font-family: Arial, sans-serif;
        background-color: #f8f8f8; 
        color: #333;
        padding-top: 60px; 
    }
    nav.navbar {
        width: 100%;
        position: fixed;
        top: 0;
        z-index: 1000;
    }
    .navbar-collapse {
        justify-content: center;
    }
    .movies {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        gap:100px;
        padding: 20px;
        margin-top: 60px; 
        width: 100%; 
    }
    .movie {
        background-color: #fff;
        box-shadow: 0 2px 4px rgba(0,0,0,0.1);
        margin: 10px; 
        text-align: center;
    }
    .movie img {
        max-width: 100%;
        height: auto;
    }
    .card-img-top {
        width: 100%;
        height: 15vw; 
        object-fit: cover; 
    }
</style>


</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.php">Search</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="home.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" aria-disabled="true">Disabled</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <?php
    // Comme la methode file get content ne marchait pas, j'ai du me renseigné pour trouver un autre moyen afin de continuer l'exercice.
if (isset($_GET["titre_filme"])) {
    $titreFilme = htmlspecialchars($_GET["titre_filme"]);
    $mot = urlencode($titreFilme);

    $pageUrl = "https://www.imdb.com/find?s=tt&q=" . $mot;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $pageUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $contenuPage = curl_exec($ch);
    curl_close($ch);

    $regexTitre = '/<a class="ipc-metadata-list-summary-item__t" .*? href="\/title\/(tt\d+)\/\?ref_=fn_tt_tt_.*?">(.*?)<\/a>/';
    $regexImages = '/<img alt=".*?" class="ipc-image" loading="lazy" src="(.*?)"/';

    if (preg_match_all($regexTitre, $contenuPage, $matchesTitres) &&
        preg_match_all($regexImages, $contenuPage, $matchesImages)) {
        
        echo "<div class='movies'>";
        $count = min(count($matchesTitres[0]), count($matchesImages[0]));
        for($i = 0; $i < 5; $i++) {
            $title = $matchesTitres[2][$i];
            $id = $matchesTitres[1][$i];
            $imageSrc = $matchesImages[1][$i];
            $encodedTitle = urlencode($title);
            $encodedImageSrc = urlencode($imageSrc);

            echo "<div class='card' style='width: 18rem;'>";
            echo "<img src='{$imageSrc}' class='card-img-top' alt='Image de {$title}'>";
            echo "<div class='card-body'>";
            echo "<h5 class='card-title'>{$title}</h5>";
            echo "<p class='card-text'>Cliquez pour plus de détails.</p>";
            echo "<a href='moovie.php?id={$id}&title={$encodedTitle}&image={$encodedImageSrc}' class='btn btn-primary'>Détails</a>";
            echo "</div>";
            echo "</div>";
        }
        echo "</div>";
        
    } else {
        echo "<p>Aucun résultat trouvé pour '" . htmlspecialchars($titreFilme) . "'.</p>";
    }
}
?>

</body>
</html>
