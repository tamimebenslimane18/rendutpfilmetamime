<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- Intégration de Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body, html {
            height: 100%;
            margin: 0;
            padding: 0;
            display: flex;
            flex-direction: column;
            align-items: center;
            background-color: #f8f8f8; 
        }

        nav.navbar {
            width: 100%;
        }

        .search-container {
            flex-grow: 1;
            width: 100%;
            margin-top: 400px;
            margin-left: 120px;
        }

        input[type="text"] {
            width: 50%; 
            padding: 10px;
            margin-top: 20px;
            box-sizing: border-box; 
        }

        button {
            padding: 10px 20px;
            margin-top: 10px;
            margin-bottom: 15px;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#"></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="home.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" aria-disabled="true"></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="search-container">
        <form action="liste.php" method="GET">
            <input type="text" id="titre_filme" name="titre_filme" placeholder="Entrez le titre d'un film">
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

    
</body>
</html>
